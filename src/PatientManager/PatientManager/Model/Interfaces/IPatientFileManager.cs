﻿using System.Collections.Generic;

namespace PatientManager.Model.Interfaces
{
    /// <remarks>
    ///     This could be generic, but currently it would be overengineering.
    /// </remarks>
    public interface IPatientFileManager
    {
        IEnumerable<Patient> LoadPatients();
        void SavePatients(IEnumerable<Patient> patients);
    }
}