﻿using PatientManager.Utilities;

namespace PatientManager.Model
{
    public sealed class Patient : Bindable
    {
        private string name;

        public string Name
        {
            get => name;
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged();
                }
            }
        }

        public void OverwriteMergeFrom(Patient other)
        {
            Name = other.Name;
        }

        public static Patient Clone(Patient other)
        {
            return new Patient()
            {
                Name = other.name
            };
        }
    }
}