﻿using System.Windows;

namespace PatientManager.View
{
    public partial class AddEditPatientWindow : Window
    {
        public AddEditPatientWindow(object viewModel)
        {
            DataContext = viewModel;
            InitializeComponent();
        }
    }
}