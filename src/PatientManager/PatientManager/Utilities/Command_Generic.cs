﻿using System;
using System.Windows.Input;

namespace PatientManager.Utilities
{
    public sealed class Command<T> : ICommand
    {
        public event EventHandler CanExecuteChanged = delegate { };

        private readonly Action<T> executeMethod;
        private readonly Func<T, bool> canExecuteMethod;

        public Command(Action<T> executeMethod)
        {
            this.executeMethod = executeMethod;
        }

        public Command(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
            : this(executeMethod)
        {
            this.canExecuteMethod = canExecuteMethod;
        }

        bool ICommand.CanExecute(object parameter)
        {
            if (canExecuteMethod != null)
                return canExecuteMethod((T)parameter);

            if (executeMethod != null)
                return true;

            return false;
        }

        void ICommand.Execute(object parameter) => executeMethod?.Invoke((T)parameter);

        public void RaiseCanExecuteChanged() => CanExecuteChanged(this, EventArgs.Empty);
    }
}