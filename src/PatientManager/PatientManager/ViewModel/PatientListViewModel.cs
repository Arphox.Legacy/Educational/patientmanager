﻿using PatientManager.Model;
using PatientManager.Model.Interfaces;
using PatientManager.Utilities;
using PatientManager.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace PatientManager.ViewModel
{
    public sealed class PatientListViewModel : Bindable
    {
        public ObservableCollection<Patient> Patients
        {
            get => patients;
            set
            {
                patients = value;
                RaisePropertyChanged();
            }
        }
        public ICommand AddCommand { get; set; }
        public Command EditCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        private ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        private Patient selectedPatient;
        private IPatientFileManager patientFileManager;

        public Patient SelectedPatient
        {
            get => selectedPatient;
            set
            {
                selectedPatient = value;
                EditCommand.RaiseCanExecuteChanged();
                DeleteCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged();
            }
        }

        public PatientListViewModel()
        {
            patientFileManager = new PatientFileManager();
            LoadExamplePatients();

            AddCommand = new Command(() => AddPatient());
            DeleteCommand = new Command(() => Patients.Remove(SelectedPatient), () => SelectedPatient != null);
            EditCommand = new Command(EditPatient, () => SelectedPatient != null);
            LoadCommand = new Command(Load);
            SaveCommand = new Command(Save);
        }

        public void LoadExamplePatients()
        {
            Patients.Add(new Patient() { Name = $"J.K. Rowling" });
            Patients.Add(new Patient() { Name = $"Judi Picoult" });
            Patients.Add(new Patient() { Name = $"Andrew Horowitz" });
        }

        private void AddPatient()
        {
            var patient = new Patient();
            var viewModel = new AddEditPatientWindowViewModel(patient, false);
            var window = new AddEditPatientWindow(viewModel);

            viewModel.CloseRequest += (sender, isSave) =>
            {
                window.Close();

                if (isSave)
                    Patients.Add(patient);
            };

            window.ShowDialog();
        }

        private void EditPatient()
        {
            var viewModel = new AddEditPatientWindowViewModel(SelectedPatient, true);
            var window = new AddEditPatientWindow(viewModel);

            viewModel.CloseRequest += (sender, isSave) =>
            {
                window.Close();

                if (isSave)
                    RaisePropertyChanged(nameof(SelectedPatient));
            };

            window.ShowDialog();
        }

        private void Load()
        {
            Patients = new ObservableCollection<Patient>(patientFileManager.LoadPatients());
        }

        private void Save()
        {
            patientFileManager.SavePatients(Patients);
            MessageBox.Show("Patients data saved successfully.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}