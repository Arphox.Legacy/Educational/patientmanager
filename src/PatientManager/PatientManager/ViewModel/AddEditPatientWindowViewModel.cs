﻿using PatientManager.Model;
using PatientManager.Utilities;
using System;
using System.Windows;
using System.Windows.Input;

namespace PatientManager.ViewModel
{
    public sealed class AddEditPatientWindowViewModel
    {
        public event EventHandler<bool> CloseRequest;
        public ICommand SaveCommand { get; set; }
        public Patient Patient { get; }
        public bool IsEdit { get; }
        public string Title => IsEdit ? "Edit patient" : "Add patient";

        private Patient originalPatient;

        public AddEditPatientWindowViewModel(Patient patient, bool isEdit)
        {
            originalPatient = patient ?? throw new ArgumentNullException(nameof(patient));
            Patient = Patient.Clone(patient);

            IsEdit = isEdit;
            SaveCommand = new Command(Save);
        }

        private void Save()
        {
            if (!IsValid())
                return;

            originalPatient.OverwriteMergeFrom(Patient);
            CloseRequest?.Invoke(this, true);
        }

        private bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(Patient.Name))
            {
                MessageBox.Show("Patient name cannot be empty.", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return false;
            }

            return true;
        }
    }
}